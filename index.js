let path = require('path'),
    nodegit = require('nodegit'),
    clone = nodegit.Clone.clone,
    userpassPlaintextNew = nodegit.Cred.userpassPlaintextNew,
    config = require('./pdm.js'),
    mkdirp = require('mkdirp');


//var argv = require('yargs')
//    .usage('Usage: $0 <command> [options]')
//    //.command('count', 'Count the lines in a file')
//    //.example('$0 count -f foo.js', 'count the lines in the given file')
//    //.alias('f', 'file')
//    //.nargs('f', 1)
//    //.describe('f', 'Load a file')
//    //.demand(1, ['f'])
//    .command('update', 'Update')
//    .help('h')
//    .alias('h', 'help')
//    .epilog('copyright 2015')
//    .argv;

let _branches_registry = [];

Object.keys(config.branches).forEach(branch => {
    config.branches[branch]['name'] = branch;
    _branches_registry.push(branch);
});

var argv = require('yargs')
    .command({
        command: 'update [branch]',
        //aliases: ['upd'],
        desc: 'Update repository',
        handler: argv => {
            let branch = argv.branch || null;

            (branch ? [branch] : _branches_registry).forEach(branch => {
                let _branch;
                if (_branch = config.branches[branch]) {
                    updateBranch(_branch);
                } else {
                    throw new Error('Implement non existing branch error');
                }
            });

        }
    })
    .demand(1)
    .help('h')
    .alias('h', 'help')
    .wrap(72)
    .argv;

function updateBranch(branch) {
    mkdirp.sync(branch.directory);
    var project = branch.directory,
        options = {
            checkoutBranch: branch.name,
            remoteCallbacks: {
                credentials: function () {
                    return userpassPlaintextNew(config.git.login, config.git.password);
                }
            },
            certificateCheck: function () {
                return 1;
            }
        };

    console.log('opts',project,options);

    function url() {
        var url = 'https://';
        url += encodeURIComponent(config.git.login);
        url += ':';
        url += encodeURIComponent(config.git.password);
        url += '@' + config.git.project;
        return url;
    }

    console.log('URL',url());

    clone(url(), project, options).then(function (repo) {
         console.log('repo:', repo);
    });


    var repository;

    nodegit.Repository.open(project)
        .then(function (repo) {
            repository = repo;

            return repository.fetchAll(options);
        })

        .then(function () {
            return repository.mergeBranches(branch.name, 'origin/' + branch.name);
        })

        .done(function () {
            console.log("Done!");
        });

}
