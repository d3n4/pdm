var ftp = {
    login: "",
    password: "",
    host: ""
};

module.exports = {
    branches: [
        {
            name: "master",
            directory: "./projects/mysite.local",
            ftp: {
                login: ftp.login,
                password: ftp.password,
                host: ftp.host,
                directory: "./htdocs/master"
            }
        },
        {
            name: "production",
            directory: "./projects/prod.mysite.local",
            ftp: {
                login: ftp.login,
                password: ftp.password,
                host: ftp.host,
                directory: "./htdocs/prod"
            }
        }
    ],
    git: {
        login: "",
        password: "",
        project: "d3n4/deploy_test"
    }
};
